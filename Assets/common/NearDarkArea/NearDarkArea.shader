﻿Shader "common/NearDarkArea"
{

	Properties
	{
		_MaxDark ("MaxDark", Range(0.0, 1.0)) = 0.5
		_Near ("Near", Float) = 5
		_Far ("Far", Float) = 10
	}

	SubShader 
	{
        Tags {"Queue"="Overlay+1"}
		ZTest Always
		Cull Front
		Pass
		{
			Blend SrcAlpha OneMinusSrcAlpha
			CGPROGRAM
			#pragma vertex vert_img
			#pragma fragment frag

			#include "UnityCG.cginc"
			
			float _MaxDark;
			float _Near;
			float _Far;

			fixed4 frag (v2f_img i) : COLOR 
			{
				float4 pos = mul(unity_ObjectToWorld, float4(0, 0, 0, 1));
				float far = length(pos.xyz - _WorldSpaceCameraPos);
				float darkness = clamp(0, 1 - (far - _Near)/(_Far - _Near), _MaxDark);
				return fixed4(0, 0, 0.0, darkness);
			}
			ENDCG
		}
	}
}
